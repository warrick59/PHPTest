<!doctype html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="php.css">
  <link rel="stylesheet" type="text/css" media="screen" href="media.css" />
  <meta charset="iso-8859">
  <title>penduAPP: Interface d'admin</title>
</head>

<body>
  <div class="titre">
    <div class="pendu">Le Pendu </div>
  </div>

  <div class="content">

    <div class="mot">

      <div class="listemot">
        <?php
        ini_set("display_errors",1);

        $handle = mysqli_connect("localhost","root","warrick59","penduAPP");
        $query = "SELECT * FROM mots";
        $result = mysqli_query($handle,$query);
        while($line =mysqli_fetch_array($result)) {

          echo $line["id"] ." ". $line["mot"];
          echo "<a href=\"delete_mot.php?id=". $line["id"] . "\"> X</a>";
          echo "<form class=\"modif\" action=\"modifiedmot.php?id=" . $line["id"] . "\" method=\"post\" >";
          echo "<input type=\"text\" name=\"update\" placeholder=\"Modifier\">";
          echo "<input type=\"submit\">";
          echo "</form>";

        }
        ?>
      </div><!--fin de listemot-->

      <div class="addmot">
        <form action="creer_mots.php" method="post">
          <label for="mot">Ajouter un mot </label>
          <input class="input" type="text" name="mot">
          <input type="submit">
        </form>
      </div><!--fin de addmot-->
    </div><!--fin de mot-->

    <div class="joueur">

      <div class="listejoueur">
        <?php
        ini_set("display_errors",1);

        $handle = mysqli_connect("localhost","root","warrick59","penduAPP");
        $query = "SELECT * FROM joueurs";
        $result = mysqli_query($handle,$query);
        while($line =mysqli_fetch_array($result)) {

          echo $line["id"] ." ". $line["nom"];
          echo "<a href=\"delete_joueurs.php?id=". $line["id"] . "\"> X</a>";
          echo "<form class=\"modif\" action=\"modifiedjoueur.php?id=" . $line["id"] . "\" method=\"post\" >";
          echo "<input  type=\"text\" name=\"update\" placeholder=\"Modifier\">";
          echo "<input type=\"submit\">";
          echo "</form>";
        }

        ?>
      </div><!--fin de listejoueur-->

      <div class="addjoueur">
        <form action="creer_joueurs.php" method="post">
          <label for="joueur">Ajouter un joueur</label>
          <input class="input" type="text" name="joueur">
          <input type="submit">
        </form>
      </div><!--fin de addjoueur-->

    </div><!--fin de joueur-->
  </div><!--fin de content-->

  <div class="bouton">
    <div class="aff"> <a href="reset.php?">Réinisaliser</a></div>
  </div>

</body>
</html>
